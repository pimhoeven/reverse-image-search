#!/bin/bash

DIR="/cloudsql"
if [ -d "$DIR" ]; then
  ### Take action if $DIR exists ###
  echo "found /cloudsql"
else
  ###  Control will jump here if $DIR does NOT exists ###
  echo "creating /cloudsql ${DIR}..."
  mkdir /cloudsql
  chown -R $USER /cloudsql
  echo "dir made"
  # exit 1
fi

echo "starting proxy"
./cloud_sql_proxy -dir=/cloudsql --instances=corded-gear-245814:us-central1:revimg --credential_file=corded-gear-245814-f324ca32071b.json