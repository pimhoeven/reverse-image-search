import csv

if __name__ == "__main__":
    counter = 0
    print('started parsing file ...')
    initial_mark = 10000

    with open('fall11_urls.csv', mode='a') as write_file:
      writer = csv.DictWriter(write_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL, fieldnames=['img_id", "url'])
      writer.writeheader()
      with open('fall11_urls.txt', encoding='utf-8', errors='ignore') as infile:
        for line in infile:
          if(counter <= initial_mark):
            if counter % (initial_mark * 0.1) == 0:
              print(f'checkmark: {counter}')
          else:
            initial_mark = initial_mark * 10
          splitted = [x.strip() for x in line.split()]
          writer.writerow({'img_id':splitted[0], 'url':splitted[1]})
          counter += 1

    print('completed parsing')
