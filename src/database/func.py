from typing import Dict, List
from connection import get_connection
import logging as log
from reverse_image_search import config

CURR_CONNECTION = None
log.basicConfig(level=log.INFO)


def read_image(id:str, conn) ->Dict:
    cursor = conn.cursor()
    log.info(f'Getting information for imgage id: {id}')
    cursor.execute("SELECT * FROM img_recognition WHERE img_id='{id}';".format(id = id))
    columns = [col[0] for col in cursor.description]
    rows = [dict(zip(columns, row)) for row in cursor.fetchall()]
    if len(rows) == 0:
        log.info(f'Image id: {id} is not valid')
        return []
    return rows[0]


def get_all_images(conn) -> List[Dict]:
    cursor = conn.cursor()
    log.info('Getting all available images features')
    cursor.execute("SELECT * FROM img_recognition;")
    columns = [col[0] for col in cursor.description]
    rows = [dict(zip(columns, row)) for row in cursor.fetchall()]
    return rows


def get_image_ids_for_feature(feature:str, conn) -> List[str]:
    cursor = conn.cursor()
    log.info(f'Getting all image id for feature: {feature}')
    cursor.execute("SELECT img_id FROM img_recognition WHERE {feature}=1;".format(feature= feature))
    rows = [row[0] for row in cursor.fetchall()]
    return rows
