import psycopg2
from reverse_image_search.config import db_host, db_user, db_password, db_database

def get_connection():
  return psycopg2.connect(
    database=db_database,
    user = db_user,
    password = db_password,
    host = db_host)