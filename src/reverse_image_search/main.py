from database.connection import get_connection
from database.func import read_image, get_image_ids_for_feature
import logging as log

CURR_CONNECTION = None
log.basicConfig(level=log.INFO)

def _initialize_connection():
  return get_connection()

if __name__ == "__main__":
    conn = _initialize_connection()
    read_image('n01322604_10275' ,conn)
    read_image('n01322604_102asds75' ,conn)
    # log.info(len(get_all_images(conn)))

    log.info(get_image_ids_for_feature('pink',conn))
  