from pyhocon import ConfigFactory
import os

conf = ConfigFactory.parse_file(f'{os.getcwd()}/config.conf')


# for logging formats you can check here: https://docs.python.org/3/library/logging.html#logrecord-attributes
# for an overall explanation check here: https://realpython.com/python-logging/
# LOGGING_FORMAT = '%(asctime)s - %(message)s'
# LOGGING_FORMAT = '%(process)d-%(levelname)s-%(message)s'
LOGGING_FORMAT = "%(asctime)s-%(levelname)s-%(message)s"

#### Database Config
db_host = os.environ.get('DB_IP', None) or conf.get_string("database.host")
db_user = conf.get_string("database.user")
db_password = conf.get_string("database.password")
db_database = conf.get_string("database.database_name")
