from typing import List, Dict
hash_type = List[float]
id_type = str
cat_type = str


def get_all_image_ids() -> List[id_type]:
    """Returns all image ids from the database"""
    # TODO: API that returns all image ids
    pass


def get_image_hashes(ids: List[id_type]) -> Dict[id_type, hash_type]:
    """

    :param ids: List of strings of image ids
    :return: Dictionary with ids the image ids and values the hashes
    """
    # TODO: API call that returns the hashes given ids
    pass


def get_image_categories(ids: List[id_type]):
    """

    :param ids: List of strings of image ids
    :return: Dictionary with ids the image ids and values the hashes
    """
    # TODO: API call that returns the hashes given a list of ids
    pass


def compare(img_hash, comparables_list, **kwargs) -> List[int]:
    """
    Compares a single image hash to a list of image hashes.

    :param img_hash: Hash of original image
    :param comparables_list: List of hashes of images to be compared
    :param kwargs:
    :return: List of similarities
    """
    pass


def get_similar_images(img_id: id_type) -> List[id_type]:
    """
    Get all ids of images that share the highest image category

    :param img_id: Image id
    :return: List of ids of similar images
    """
    # TODO: API call that returns the ids of images with the same top-level category given a single id
    pass


def get_image_categories(img_ids: List[id_type]) -> Dict[id_type, cat_type]:
    """
    Given a list of ids, return the categories according to ResNet

    :param img_ids: List of image ids
    :return: Dictionary of image_id: categories
    """
    # TODO: API call that returns the categories according to ResNet given a list of ids
    pass


def verify(img_hash, comparables_list, score_func):
    similarities = compare(img_hash, comparables_list)

    truths = get_similar_images(img_hash)

    return score_func(similarities, truths)
