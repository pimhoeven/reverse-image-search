from setuptools import setup, find_packages

setup(
    name="reverse-image-search",
    version="0.1.0",
    package_dir={"": "src"},
    packages=find_packages("src"),
    setup_requires=["pytest-runner", "flake8"],
    extras_require={
        "linting": ["mypy", "mypy-extensions", "black"],
        "develop": ["jupyter"],
        "ci_test": ["pytest", "flake8"]
    },
    install_requires=[
        "psycopg2-binary",
        "pyhocon",
        "numpy",
        "scipy",
        "pandas"
    ],
    tests_require=["pytest"],
)
